package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	testutils "github.com/openshift-kni/performance-addon-operators/functests/utils"
	testclient "github.com/openshift-kni/performance-addon-operators/functests/utils/client"
	"github.com/openshift-kni/performance-addon-operators/functests/utils/nodes"
	"github.com/openshift-kni/performance-addon-operators/functests/utils/pods"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ctnresources struct {
	cpu, memory, hpgSize, medium, noOfhpgs string
}

type mmContainer struct {
	name, image  string
	command      []string
	podResources ctnresources
}
type mmVolumes struct {
	volumenName, medium string
}

type mmPod struct {
	podV1Struct        *v1.Pod
	podName, nameSpace string
	labels             map[string]string
	podContainer       mmContainer
	podVolume          mmVolumes
}

const (
	hugepagesResourceName2Mi = "hugepages-2Mi"
	mediumHugepages2Mi       = "HugePages-2Mi"
	hugepagesResourceName1Gi = "hugepaegs-1Gi"
	mediumHugepages1Gi       = "HugePages-1Gi"
)

func main() {

	var workerNodes []v1.Node
	var err error
	//	var fooPod *v1.Pod
	workerNodes, err = nodes.GetByLabels(testutils.NodeSelectorLabels)
	workerNodes, err = nodes.MatchingOptionalSelector(workerNodes)
	if err != nil {
		fmt.Println("something bad happened")
	}

	var testPod mmPod
	testPod.podName = "example1"
	testPod.nameSpace = "default"
	testPod.podContainer.name = fmt.Sprintf("%s-container", "example")
	testPod.podContainer.image = "fedora:latest"
	testPod.podContainer.command = []string{"sleep", "inf"}
	testPod.podContainer.podResources.cpu = "2"
	testPod.podContainer.podResources.memory = "200Mi"
	testPod.podContainer.podResources.hpgSize = strings.ToLower(hugepagesResourceName2Mi)
	testPod.podContainer.podResources.medium = mediumHugepages2Mi
	testPod.podContainer.podResources.noOfhpgs = "24Mi"

	mypod := mmPodTemplate(&testPod, &workerNodes[0])
	err = testclient.Client.Create(context.TODO(), mypod)
	if err != nil {
		fmt.Println("unable to create pod")
		fmt.Println(err)
	}
	err = pods.WaitForCondition(mypod, v1.PodReady, v1.ConditionTrue, 10*time.Minute)
}

func createCtnResources(ctn *mmContainer) *v1.ResourceRequirements {

	ctnLimits := v1.ResourceList{
		v1.ResourceCPU:    resource.MustParse(ctn.podResources.cpu),
		v1.ResourceMemory: resource.MustParse(ctn.podResources.memory),
	}
	if ctn.podResources.hpgSize == "hugepages-2mi" {
		ctnLimits[hugepagesResourceName2Mi] = resource.MustParse(ctn.podResources.noOfhpgs)
	}
	return &v1.ResourceRequirements{
		Limits: ctnLimits,
	}
}

func createContainer(ctnData *mmContainer) *v1.Container {
	mmctn := v1.Container{
		Name:      ctnData.name,
		Image:     ctnData.image,
		Command:   ctnData.command,
		Resources: *createCtnResources(ctnData),
		VolumeMounts: []v1.VolumeMount{
			*createVolumeMounts(ctnData),
		},
	}
	return &mmctn
}

func createVolumeMounts(ctnData *mmContainer) *v1.VolumeMount {
	if ctnData.podResources.hpgSize == "hugepages-2mi" {
		return &v1.VolumeMount{
			Name:      "hugepages-2mi",
			MountPath: "/hugepages-2Mi",
		}
	} else {
		return &v1.VolumeMount{
			Name:      "hugepage-1gi",
			MountPath: "/hugepages-1Gi",
		}
	}
}

func mmPodTemplate(testPod *mmPod, targetNode *v1.Node) *v1.Pod {
	testNode := make(map[string]string)
	testNode["kubernetes.io/hostname"] = targetNode.Name
	testPod.podV1Struct = &v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-pod", testPod.podName),
			Namespace: testPod.nameSpace,
			Labels: map[string]string{
				"name": testPod.podName,
			},
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				*createContainer(&testPod.podContainer),
			},
			Volumes: []v1.Volume{
				{
					Name: testPod.podContainer.podResources.hpgSize,
					VolumeSource: v1.VolumeSource{
						EmptyDir: &v1.EmptyDirVolumeSource{
							Medium: v1.StorageMedium(testPod.podContainer.podResources.medium),
						},
					},
				},
			},
			NodeSelector: testNode,
		},
	}
	return testPod.podV1Struct
}
