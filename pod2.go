package main

import (
	"context"
	"fmt"
	"time"

	testutils "github.com/openshift-kni/performance-addon-operators/functests/utils"
	testclient "github.com/openshift-kni/performance-addon-operators/functests/utils/client"
	"github.com/openshift-kni/performance-addon-operators/functests/utils/nodes"
	"github.com/openshift-kni/performance-addon-operators/functests/utils/pods"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	hugepagesResourceName2Mi = "hugepages-2Mi"
	mediumHugepages2Mi       = "HugePages-2Mi"
)

func main() {
	var testpod *v1.Pod
	var err error
	var workerRTNodes []v1.Node
	workerRTNodes, err = nodes.GetByLabels(testutils.NodeSelectorLabels)
	workerRTNodes, err = nodes.MatchingOptionalSelector(workerRTNodes)
	if err != nil {
		fmt.Println("Something bad happened")
	}
	testpod = podTemplate("example1", testpod, &workerRTNodes[0])
	err = testclient.Client.Create(context.TODO(), testpod)
	if err != nil {
		fmt.Println("unable to create pod")
	}
	err = pods.WaitForCondition(testpod, v1.PodReady, v1.ConditionTrue, 10*time.Minute)

}

func podTemplate(name string, testpod *v1.Pod, targetNode *v1.Node) *v1.Pod {
	podNode := make(map[string]string)
	podNode["kubernetes.io/hostname"] = targetNode.Name
	testpod = &v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-pod", name),
			Namespace: "default",
			Labels: map[string]string{
				"name": name,
			},
		},
		// Pod specification
		Spec: v1.PodSpec{
			//containers to run in pod
			Containers: []v1.Container{
				{
					Name:    fmt.Sprintf("%s-container", name),
					Image:   "fedora:latest",
					Command: []string{"sleep", "inf"},
					Resources: v1.ResourceRequirements{
						Limits: v1.ResourceList{
							v1.ResourceCPU:           resource.MustParse("2"),
							v1.ResourceMemory:        resource.MustParse("200Mi"),
							hugepagesResourceName2Mi: resource.MustParse("24Mi"),
						},
					},
					VolumeMounts: []v1.VolumeMount{
						{
							Name:      "hugepages-2mi",
							MountPath: "/hugepages-2Mi",
						},
					},
				},
			},
			Volumes: []v1.Volume{
				{
					Name: "hugepages-2mi",
					VolumeSource: v1.VolumeSource{
						EmptyDir: &v1.EmptyDirVolumeSource{
							Medium: mediumHugepages2Mi,
						},
					},
				},
			},
			NodeSelector: podNode,
		},
	}
	return testpod
}
