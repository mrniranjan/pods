package main

import (
	"context"
	"fmt"
	testclient "github.com/openshift-kni/performance-addon-operators/functests/utils/client"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

//main function
func main() {

	// create a varible of type v1.Pod
	var testpod *v1.Pod
	var err error
	//Define Pod
	testpod = &v1.Pod{
		//Define Metadata
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test-pod", //Pod name: Explicity specified.
			Namespace: "default",  // The name space where pod should be created
			Labels: map[string]string{
				"name": "test-pod",
			},
		},
		//Pod specification
		Spec: v1.PodSpec{
			// Containers to run in Pod
			Containers: []v1.Container{
				{
					Name:    "test-fedora-container",  // container name
					Image:   "fedora:latest",          //container image fedora:latest
					Command: []string{"sleep", "inf"}, // Command that runs which is sleep
				},
			},
		},
	}
	err = testclient.Client.Create(context.TODO(), testpod)
	if err != nil {
		fmt.Println("Pod did not get created")
	}
}
