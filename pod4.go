package main

import (
	"context"
	"fmt"
	"time"

	testutils "github.com/openshift-kni/performance-addon-operators/functests/utils"
	testclient "github.com/openshift-kni/performance-addon-operators/functests/utils/client"
	"github.com/openshift-kni/performance-addon-operators/functests/utils/nodes"
	"github.com/openshift-kni/performance-addon-operators/functests/utils/pods"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ctnResources struct {
	cpu, memory, hugepageResource, hpgSize string
}

type mmContainer struct {
	name, image  string
	command      []string
	podResources ctnResources
}

const (
	hugepagesResourceName2Mi = "hugepages-2Mi"
	mediumHugepages2Mi       = "HugePages-2Mi"
)

func main() {

	var workerNodes []v1.Node
	var err error
	var fooPod *v1.Pod
	workerNodes, err = nodes.GetByLabels(testutils.NodeSelectorLabels)
	workerNodes, err = nodes.MatchingOptionalSelector(workerNodes)
	if err != nil {
		fmt.Println("something bad happened")
	}

	var ctn mmContainer
	ctn.name = fmt.Sprintf("%s-container", "example")
	ctn.image = "fedora:latest"
	ctn.command = []string{"sleep", "inf"}
	ctn.podResources.cpu = "2"
	ctn.podResources.memory = "200Mi"
	ctn.podResources.hugepageResource = hugepagesResourceName2Mi
	ctn.podResources.hpgSize = "24Mi"

	mypod := PodTemplate("example", fooPod, &workerNodes[0], &ctn)
	err = testclient.Client.Create(context.TODO(), mypod)
	if err != nil {
		fmt.Println("unable to create pod")
		fmt.Println(err)
	}
	err = pods.WaitForCondition(mypod, v1.PodReady, v1.ConditionTrue, 10*time.Minute)
}

func createCtnResources(ctn *mmContainer) *v1.ResourceRequirements {

	ctnLimits := v1.ResourceList{
		v1.ResourceCPU:    resource.MustParse(ctn.podResources.cpu),
		v1.ResourceMemory: resource.MustParse(ctn.podResources.memory),
	}
	if ctn.podResources.hugepageResource == "hugepages-2Mi" {
		ctnLimits[hugepagesResourceName2Mi] = resource.MustParse(ctn.podResources.hpgSize)
	}
	return &v1.ResourceRequirements{
		Limits: ctnLimits,
	}
}

func createContainer(ctnData *mmContainer) *v1.Container {
	mmctn := v1.Container{
		Name:      ctnData.name,
		Image:     ctnData.image,
		Command:   ctnData.command,
		Resources: *createCtnResources(ctnData),
		VolumeMounts: []v1.VolumeMount{
			{
				Name:      "hugepages-2mi",
				MountPath: "/hugepages-2Mi",
			},
		},
	}
	return &mmctn
}

func PodTemplate(name string, fooPod *v1.Pod, targetNode *v1.Node, ctnData *mmContainer) *v1.Pod {
	fooNode := make(map[string]string)
	fooNode["kubernetes.io/hostname"] = targetNode.Name
	fooPod = &v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-pod", name),
			Namespace: "default",
			Labels: map[string]string{
				"name": name,
			},
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				*createContainer(ctnData),
			},
			Volumes: []v1.Volume{
				{
					Name: "hugepages-2mi",
					VolumeSource: v1.VolumeSource{
						EmptyDir: &v1.EmptyDirVolumeSource{
							Medium: mediumHugepages2Mi,
						},
					},
				},
			},
			NodeSelector: fooNode,
		},
	}
	return fooPod
}
